<?php
class TasksController extends CController
{
	public $win_title='Задачи';
	const PAGE_SIZE=10;
	private $_model;
	public $layout="internal";
	
	public function filters(){
		return array(
			'accessControl', // perform access control for CRUD operations
		);
	}

	public function accessRules(){
		return array(
			array('allow',  // allow all users to perform 'list' and 'show' actions
				'actions'=>array('HelpDeskTaskCreate'),
				'users'=>array('*'),
			),
			array('allow', // allow authenticated users to access all actions
				'users'=>array('@'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}


	public function actionIndex(){
		if (!isset($_GET['timestamp'])) $currenttimestamp = time();
		else $currenttimestamp = $_GET['timestamp'];

		$dataProvider=new CActiveDataProvider('Tasks', array(
						'criteria'=>array(
							'condition'=>"id_owner=:currentorg  AND (startdate < :currentstopdate AND (stopdate > :currentstartdate OR stopdate = '0000-00-00 00:00:00'))",
							'params'=>array(':currentstartdate'=>date('Y-m-d', $currenttimestamp).' 00:00:00',
											':currentstopdate'=>date('Y-m-d', $currenttimestamp).' 23:59:59',
											':currentorg'=>Yii::app()->user->org,
											),
						),
    			'pagination'=>array(
        				'pageSize'=>20,
    			),
		));


		$this->win_title='Все задачи на '.date('d.m.Y',$currenttimestamp);
		$this->render('index',array('dataProvider'=>$dataProvider));
	}

	public function actionCreate(){
		$model=new Tasks;
		$this->win_title.=' Новая';
		
		if (!isset($_GET['timestamp'])) $currenttimestamp = time();
		else $currenttimestamp = $_GET['timestamp'];
		
		$model->startdate = date('Y-m-d H:i:s',$currenttimestamp);
		$model->sendsms=1;
		
		if(isset($_POST['Tasks'])){
			$model->attributes = $_POST['Tasks'];
			$model->startdate = $_POST['startdate'].' '.$_POST['starttime']; //Старт задания
			$model->executedate = $_POST['executedate'].' '.$_POST['executetime']; //Дата исполнения
			$model->id_author = Yii::app()->user->getId(); //Автор задания текущий пользователь
			$model->id_owner = Yii::app()->user->org;
			$model->ex_description='';
			if($model->save()){
				if ($model->id_executor && $model->sendsms){
					//если установлен исполнитель и галочка отправки, отправляем SMS сообщение
					$textmsg = $model->kontr->name."\n";
					$textmsg.= "Адрес: ".$model->kontr->addr."\n";
					$textmsg.= $model->description."\n";
			
					Glob::sendUserMsg($model->id_executor,$textmsg);
				}
				$this->redirect(Yii::app()->user->returnUrl);
			}
		}
		$this->render('create',array('model'=>$model));
	}

	public function actionHelpDeskTaskCreate(){  //задачи от хелп деска новая версия
		
		//Формируем сообщение 
		$textmsg = "****Comty.ru****\n\n";
		$desc = $_POST['desc'];
		
		//Ищем компьютер по IP адресу отправителя
		if ($model = Computers::model()->getByIP($_SERVER['REMOTE_ADDR'])){

		    //Если найден компьютер 
		    //Определяем контору которой принадлежит данный комп
		    $textmsg.= $model->org->name."\n";
		    $textmsg.= "Адрес: ".$model->org->addr."\n";
		    $textmsg.= "Компьютер: ".$model->name."\n";
		    $textmsg.= "IP-адрес: ".$_SERVER['REMOTE_ADDR']."\n";
		    
		    //Проверяем владельца компьютера
		    if ($model->id_owner != 0){
			$textmsg.= "Отправитель: ".$model->owner->name."\n";
			$textmsg.= "Телефон: ".$model->owner->phone."\n";
		    }else
			$textmsg.= "Отправитель: Нет данных \n"; //Если нет то говорим что комп без хозяина
		
		    $textmsg.= "\n".$desc."\n";
		
		//Создаем задачу с нужными параметрами
		$task=new Tasks;
		$task->startdate = date('Y-m-d H:i:s',time());
		$task->description = $desc;
		$task->id_executor = '';
		$task->id_kontragent = $model->id_org;
		$task->ex_description='';
		$task->save();
		
		//Oповещаем ответственных
		    foreach($model->org->otv as $item){
			Glob::sendUserMsg($item->id_user,$textmsg);
		    }
		
		}
		else{
		    //Если компьютер не найден в базе то добавляем только IP адрес
		    //Желательно чтобы так-же создавал задачу для деспечера чтобы заполнила базу
		
		//Создаем задачу с нужными параметрами
		$model=new Tasks;
		$model->startdate = date('Y-m-d H:i:s',time());
		$model->description = $desc.$_SERVER['REMOTE_ADDR'];
		$model->id_executor = '';
		$model->id_kontragent = 0;
		$model->ex_description='';
		$model->save();
		
		}
	}

	public function actionUpdate(){
		$model=$this->loadModel();
		$this->win_title='Редактирование задачи';

			if(isset($_POST['Tasks'])){
					$model->attributes = $_POST['Tasks'];
					//if($model->validate()){
					
					$model->startdate = $_POST['startdate'].' '.$_POST['starttime']; //Старт задания
					$model->executedate = $_POST['executedate'].' '.$_POST['executetime']; //Дата исполнения
					
					if($model->save()){
							
						if ($model->id_executor && $model->sendsms){
						    //если установлен исполнитель и галочка отправки, отправляем SMS сообщение
							$textmsg = $model->kontr->name."\n";
							$textmsg.= "Адрес: ".$model->kontr->addr."\n";
							$textmsg.= $model->description."\n";
			
							Glob::sendUserMsg($model->id_executor,$textmsg);
						}
						$this->redirect(Yii::app()->user->returnUrl);
						}
					//}
				}

			$this->render('update',array('model'=>$model));
	}

	public function actionDelete(){
		$this->loadModel()->delete();
		$this->redirect(Yii::app()->user->returnUrl);
	}

	public function actionIndexByExecutor(){
		Yii::app()->user->setReturnUrl(Yii::app()->getRequest()->getUrl()); //Запоминаем текущую страницу для редиректа
	
		if (!isset($_GET['timestamp'])) $currenttimestamp = time();
		else $currenttimestamp = $_GET['timestamp'];

		$q = new CDbCriteria( array(
							'condition'=>"id_owner=:currentorg  AND (startdate < :currentstopdate AND (stopdate > :currentstartdate OR stopdate = '0000-00-00 00:00:00'))",
							'order'=>'id_executor',
							'params'=>array(':currentstartdate'=>date('Y-m-d', $currenttimestamp).' 00:00:00',
											':currentstopdate'=>date('Y-m-d', $currenttimestamp).' 23:59:59',
											':currentorg'=>Yii::app()->user->org,
											),
		));

		$model=Tasks::model()->findAll($q);
		
		if (!isset($_GET['print'])){
			$this->win_title='Все задачи (по исполнителю) на '.date('d.m.Y',$currenttimestamp);
			$this->render('indexbyexecutor',array('model'=>$model));
		}
		else
			$this->renderPartial('printbyexecutor',array('model'=>$model));
			
	}

	public function actionIndexByOrg(){
		Yii::app()->user->setReturnUrl(Yii::app()->getRequest()->getUrl()); //Запоминаем текущую страницу для редиректа
		
		if (!isset($_GET['timestamp'])) $currenttimestamp = time();
		else $currenttimestamp = $_GET['timestamp'];

		$q = new CDbCriteria( array(
							'condition'=>"id_owner=:currentorg  AND (startdate < :currentstopdate AND (stopdate > :currentstartdate OR stopdate = '0000-00-00 00:00:00'))",
							'order'=>'id_kontragent',
							'params'=>array(':currentstartdate'=>date('Y-m-d', $currenttimestamp).' 00:00:00',
											':currentstopdate'=>date('Y-m-d', $currenttimestamp).' 23:59:59',
											':currentorg'=>Yii::app()->user->org,
											),
		));

		$model=Tasks::model()->findAll($q);
		
		if (!isset($_GET['print'])){
			$this->win_title='Все задачи (по организации) на '.date('d.m.Y',$currenttimestamp);
			$this->render('indexbyorg',array('model'=>$model));
		}
		else
			$this->renderPartial('printbyorg',array('model'=>$model));		
	}
	
	public function actionCorrect(){
		$model=$this->loadModel();
		$model->state = 0;
		$model->save();
		$this->redirect(Yii::app()->user->returnUrl);
	}

	public function actionUploadFile(){
		$form=new UploadFileForm;
		$this->win_title='Загрузка файла';
		$id = $_GET['id'];
		
		
		if(isset($_POST['UploadFileForm'])){
		    $form->attributes = $_POST['UploadFileForm'];
		    $form->ufile = CUploadedFile::getInstance($form,'ufile');
		    $filepath = Yii::app()->basePath.'/../files/tasks/'.$id;
		    Glob::mkpath($filepath);
		    $form->ufile->saveAs($filepath.'/'.$form->ufile->name);
		    $this->redirect(Yii::app()->user->returnUrl);
		}
		
		$this->render('_form_file_upload',array('model'=>$form));
	}

	public function actionDelUploadFile($id,$filename){
		$filepath = Yii::app()->basePath.'/../files/tasks/'.$id.'/'.$filename;
		unlink($filepath);
		$this->redirect(Yii::app()->user->returnUrl);
	}

	public function actionAcceptExecute($id,$acpt){
		$model = Tasks::model()->findbyPk($id);
		$model->state = $acpt;
		$model->save();
		
		if($model->price <> 0){
			$tr = new Finance;
			$tr->sum = $model->price;
			$tr->id_user = $model->id_executor;
			$tr->description = 'Начисление по задаче № '.$model->id;
			$tr->save();
		} 
		
		$this->redirect(Yii::app()->user->returnUrl);
	}

	public function actionExecute(){//Выполнение задачи
		$model=$this->loadModel();
		$this->win_title.=' Выполнить';		
		
		if(isset($_POST['Tasks'])){
					
					$model->attributes = $_POST['Tasks'];

					if(($model->state == 1)and($model->accept_execute == 1)) $model->state = 3;
					if(($model->state == 1)or($model->state == 3)) $model->stopdate = date('Y-m-d H:i:s', time());
										
					if($model->notify_execute){
						//Уведомлять при выполнении автора задания
						Glob::sendUserMsg($model->id_owner,'Задание '.$model->id.' выполнено!');
					}

					if($model->save()){
							$this->redirect(Yii::app()->user->returnUrl);
						}
				}
				

		$this->render('execute',array('model'=>$model,));
	}
	
	public function actionPrintUserTasks(){
		
		$currenttimestamp = time();
		
		$q = new CDbCriteria( array(							
							'order'=>'id_kontragent',
							'condition'=>"startdate < :currentstopdate AND (stopdate > :currentstartdate OR stopdate = '0000-00-00 00:00:00') AND id_executor = :uid",
							'params'=>array(':currentstartdate'=>date('Y-m-d', $currenttimestamp).' 00:00:00',
											':currentstopdate'=>date('Y-m-d', $currenttimestamp).' 23:59:59',
											':uid'=>$_GET['id'],
											),
		));

		$model=Tasks::model()->findAll($q);
		
		
		$this->renderPartial('printbyorg',array('model'=>$model));
	}
	
	public function loadModel(){
		if($this->_model===null)
		{
			if(isset($_GET['id']))
				$this->_model=Tasks::model()->findbyPk($_GET['id']);
			if($this->_model===null)
				throw new CHttpException(404,'The requested page does not exist.');
		}
		return $this->_model;
	}

}