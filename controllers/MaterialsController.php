<?php

class MaterialsController extends CController{
	public $win_title='Window title';
	public $type = 3;
	public $layout="internal";
	
	public function filters(){
		return array(
			'accessControl', // perform access control for CRUD operations
		);
	}

	public function accessRules(){
		return array(
			array('allow', // allow authenticated users to access all actions
				'users'=>array('@'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}
	
	public function actions(){		
		
		return array(
			
			'creategp'=>array(
								'class'=>'application.controllers.gp.CreateGpAction',
								'type'=>$this->type,
								),
			
			'deletegp'=>array(
								'class'=>'application.controllers.gp.DeleteGpAction',
								),
		);
	}
	
	public function actionAdmin(){
		$this->win_title='Материалы';
		$this->render('admin');
	}
	
	public function actionMatIn(){
		$this->win_title='Поступление материалов';
		$form=new MaterialForm;
		
		$currentdate = date('Y-m-d h:i:s',time());
		
		if(isset($_POST['MaterialForm'])){
			$form->attributes=$_POST['MaterialForm'];
			if($form->validate()){
				for($i=0;$i<$form->cnt;$i++){
					$mat = new Materials;					
					$mat->id_sclad = $form->warehouse;
					$mat->name = $form->name;
					$mat->optprice = $form->optprice;
					$mat->price = $form->price;
					$mat->date_in = $currentdate;
					$mat->save();
				}
				$this->redirect(array('admin'));
			}
		}
		
		$this->render('matin',array('model'=>$form));
	}
	
	public function actionDelete(){
		Materials::model()->findbyPk($_GET['id'])->delete();
		$this->redirect(array('admin'));
	}
	
	public function actionTransfer(){
		$model=new TransferMaterialForm;
		$this->win_title='Передача материалов';
		
		if(isset($_POST['mat_from'])and($_POST['mat_to'])){
			$this->redirect(array('transfertest','t0'=>$_POST['mat_from'],'t1'=>$_POST['mat_to']));
		}
		
		//Первой пустим пустышку
		$list_target[] = '';
		
		//Генерим список складов
		$sk_target = ItemsGroups::model()->findAll('type = 3');
		foreach ($sk_target as $item) {		
				$list_target['0_'.$item->id] = 'Склад '.$item->name;
			}
		
		//Генерим список подотчетников
		$usr_target = Users::model()->findAll(array('order'=>'name'));
		foreach ($usr_target as $item) {		
				$list_target['1_'.$item->id] = $item->name;
			}
	
		$this->render('transfer',array('list_target'=>$list_target));
	}
	
	public function actionTransferTest(){
		$this->win_title='Передача материалов';
	
		
	
		//Чистим кеш
		ServiceCache::model()->deleteAll('id_session = :mysession',array(':mysession'=>Yii::app()->getSession()->getSessionID()));
	
		for($i=0;$i<2;$i++){
			//Разбираем идентификатор
			$nm = explode('_',$_GET['t'.$i]);//Источник1
			
			//Смотрим какие материалы уже есть у выбронного чела или склада у Источника1
			$modelmat = Materials::model()->findAll('id_sclad = '.$nm[1].' AND type_sclad = '.$nm[0]);
	
			//Переносим найденное в кеш
			foreach ($modelmat as $itemmat) {
				$cachemodel = new ServiceCache;
				$cachemodel->id_session = Yii::app()->getSession()->getSessionID();//Добавляем индификатор сессии
				$cachemodel->id_sv = $itemmat->id;
				$cachemodel->type = $i; //Подразумевает  источник1 или источник2
				$cachemodel->save();
			}
	
		}
		
			//Потом запрашиваем содержимое кеша 
			$dataProvider=new CActiveDataProvider('ServiceCache', array(
				'criteria'=>array(	
							'condition'=>"id_session = :mysession AND type = :type",
							'params'=>array(':mysession'=>Yii::app()->getSession()->getSessionID(),
											':type'=>0,
											),
						),
			));
			
			//Потом запрашиваем содержимое кеша 
			$dataProvider1=new CActiveDataProvider('ServiceCache', array(
				'criteria'=>array(	
							'condition'=>"id_session = :mysession AND type = :type",
							'params'=>array(':mysession'=>Yii::app()->getSession()->getSessionID(),
											':type'=>1,
											),
						),
			));
		
		$this->render('_transfergridtest',array('dataProvider'=>$dataProvider,'dataProvider1'=>$dataProvider1));//и рендерим представление 
	}
	
	public function actionOnChange_Target(){ //Привыборе выпадающего списка источника

		//Разбираем идентификатор
		$nm = explode('_',$_GET['id']);
	
		//Смотрим какие работы и материалы уже есть у выбронного чела или склада
		$modelmat = Materials::model()->findAll('id_sclad = :gid AND type_sclad = :tpsk',array(':gid'=>$nm[1],':tpsk'=>$nm[0]));
	
		//Чистим кеш
		ServiceCache::model()->deleteAll('id_session = :mysession AND type = :type',array(':mysession'=>Yii::app()->getSession()->getSessionID(),':type'=>$nm[2]));
	
		//Переносим найденное в кеш
		foreach ($modelmat as $itemmat) {
			$cachemodel = new ServiceCache;
			$cachemodel->id_session = Yii::app()->getSession()->getSessionID();//Добавляем индификатор сессии
			$cachemodel->id_sv = $itemmat->id;
			$cachemodel->type = $nm[2]; //Подразумевает  источник1 или источник2
			$cachemodel->save();
		}
		
		//Потом запрашиваем содержимое кеша 
		$dataProvider=new CActiveDataProvider('ServiceCache', array(
				'criteria'=>array(	
							'condition'=>"id_session = :mysession AND type = :type",
							'params'=>array(':mysession'=>Yii::app()->getSession()->getSessionID(),
											':type'=>$nm[2],
											),
						),
    			'pagination'=>array(
        				'pageSize'=>20,
    			),
		));
		
		$this->renderPartial('_transfergrid',array('dataProvider'=>$dataProvider));//и рендерим представление 
	}
	
	public function actionAddTransfer(){
	
		if($_GET['type']==1) $type=0;
		else $type=1;
	
		$cachemodel = ServiceCache::model()->findbyPk($_GET['id']);
		$cachemodel->type = $type;
		$cachemodel->save();
		
		//Потом запрашиваем содержимое кеша 
		$dataProvider=new CActiveDataProvider('ServiceCache', array(
				'criteria'=>array(	
							'condition'=>"id_session = :mysession AND type = :type",
							'params'=>array(':mysession'=>Yii::app()->getSession()->getSessionID(),
											':type'=>0,
											),
						),
    			'pagination'=>array(
        				'pageSize'=>20,
    			),
		));
	
		$dataProvider1=new CActiveDataProvider('ServiceCache', array(
				'criteria'=>array(	
							'condition'=>"id_session = :mysession AND type = :type",
							'params'=>array(':mysession'=>Yii::app()->getSession()->getSessionID(),
											':type'=>1,
											),
						),
    			'pagination'=>array(
        				'pageSize'=>20,
    			),
		));
		
		$this->renderPartial('_transfergridtest',array('dataProvider'=>$dataProvider,'dataProvider1'=>$dataProvider1));//и рендерим представление 
	}
	
	public function actionSaveTransfer(){
		
		//Разбираем идентификатор
		$nm0 = explode('_',$_GET['t0']);
		$nm1 = explode('_',$_GET['t1']);
		
		$id = array(
					'0'=> array('id'=>$nm0[1],'type'=>$nm0[0]),
					'1'=> array('id'=>$nm1[1],'type'=>$nm1[0]),
					);
	
	
		//Читаем содержимое кеша
		$cachemodel = ServiceCache::model()->findAll('id_session = :mysession',array(':mysession'=>Yii::app()->getSession()->getSessionID()));		
							
		//Записываем содержимое кеша
		foreach ($cachemodel as $itemcache){
			$modelmat = Materials::model()->findbyPk($itemcache->id_sv);
			$modelmat->id_sclad = $id[$itemcache->type]['id'];
			$modelmat->type_sclad = $id[$itemcache->type]['type'];
			$modelmat->save();
		}
		
		//Чистим кеш
		ServiceCache::model()->deleteAll('id_session = :mysession',array(':mysession'=>Yii::app()->getSession()->getSessionID()));
		
		$this->redirect(array('admin'));
		
	}
	
	public function actionAjaxFillTree(){
		if (!Yii::app()->request->isAjaxRequest) {
			exit();
		}
			
		//Только для верхнего уровня
		if ((isset($_GET['root']))and($_GET['root']=='source')) {
		
			//Запрашиваем информацию по складам
			$model=ItemsGroups::model()->findAll('type = 3');
		
			//Перебираем результат
			foreach ($model as $item) {
				$str = $item->name;
				$str .= '&nbsp;'.CHtml::link('(Удалить)',array('deletegp','id'=>$item->id));			
			
				$my_data[]=array(
								'id'=>'0_'.$item->id,
								'text' => $str,
								'expanded' => false, 
								'hasChildren'=>true
				);
			}
		
			//Тут у нас подотчетные лица
			$modelusr = Users::model()->findAll(array('order'=>'name'));
			
			//Перебираем результат
			foreach ($modelusr as $itemusr) {
				$my_data[]=array(
								'id'=>'1_'.$itemusr->id,
								'text'=>$itemusr->name,
								'hasChildren'=>true,
				);
				
			}
			
		}else{
				//Разбираем идентификатор
				$nm = explode('_',$_GET['root']);
		
				//Запрашиваем информацию по юнитам				
				$model=Materials::model()->findAll('id_sclad = :gid AND type_sclad = :tpsk',array(':gid'=>$nm[1],':tpsk'=>$nm[0]));
				
				//Перебираем результат
				foreach ($model as $item) {
					$str = $item->name;
					$str .= '&nbsp;<b>( '.$item->optprice.' / '.$item->price.' / '.date("d.m.y H:i",strtotime($item->date_in)).' )</b>';
					//$str .= '&nbsp;'.CHtml::link('(Редактировать)',array('update','id'=>$item->id));
					$str .= '&nbsp;'.CHtml::link('(Удалить)',array('delete','id'=>$item->id));			
				
					$my_data[]=array(
									'id'=>$item->id,
									'text' => $str,
					);
				}
		}
		
		echo CTreeView::saveDataAsJson($my_data);
		exit();
	}
}