<div class=<?php echo 'task_s'.$data['state']; ?><?php if(Yii::app()->controller->action->id != 'details'){?> style="margin-left:30px;"<?php }?> >
	<?php echo '<b>№&nbsp;'.$data['id'].'&nbsp;&nbsp;['.date("d.m.y H:i",strtotime($data['startdate'])).']</b>';?>

	<?php if($data['important']==1){
				echo '<BLINK><FONT color="red"><b>ВАЖНО!!!</b></FONT></BLINK>';
			}?>

	&nbsp;&nbsp;
	<b>Автор:</b> <?php if($data['id_author']!=0) echo $data['author']->person->name;
							else echo 'Бот';?>
	
	
	<?php 
	
	$action = Yii::app()->controller->action->id; //Смотрим какой у нас акшен
	
	if  (($action == 'indexbyexecutor')||($action == 'details')) { //Если просмотр по исполнителю то показываем название организации 
	
		echo '&nbsp;&nbsp; <b>Организация:</b> ';
	
		if($data['id_kontragent']!=0) 
			echo CHtml::link($data['kontr']->name, array('kontragents/details','id'=>$data['id_kontragent']));
		else 
			echo '<BLINK><FONT color="green">Неизвестно!</FONT></BLINK>';// вот тут я добавил организацию
	
	} 
	elseif  ($action == 'indexbyorg') { //Если просмотр по организации то показываем имя исполнителя
	
		echo '&nbsp;&nbsp; <b>Исполнитель:</b> ';
		
		if($data['id_executor']!=0) 
			echo CHtml::link($data['executor']->person->name, array('users/details','id'=>$data['id_executor']));
			//echo $data['executor']->person->name;
		else 
			echo '<BLINK><FONT color="green">Неназначен!</FONT></BLINK>';
	
	}

	
	?>
		
	
	&nbsp;&nbsp;
	<b>Стоимость:</b> <?php echo $data['price'];?>
	<br>
	<div style="margin-left:30px;">
	<?php if(isset($data['description'])) echo $data['description']; ?>
	<?php if ($data['ex_description']) echo '<br>&nbsp;&nbsp;<b>>>></b>&nbsp;&nbsp;'.$data['ex_description']; ?>
	</div>

	<?php 
		//Смотрим файлы для этого задания
		$path_files='w_comty/files/tasks/'.$data['id'];
		if (file_exists($path_files)){

		    if ($countfiles = count(array_filter(glob("$path_files/*"), 'is_file')) > 0){

		$d = dir($path_files);
	?>

	<b>Файлы:</b>
	<br>
	<div style="margin-left:30px;">
	<?php 
		
		
		while (false !== ($entry = $d->read())) {
		    if (($entry !== '.')and($entry !== '..')){
			    echo CHtml::link($entry,$path_files.'/'.$entry);
			    echo '&nbsp;';
			    echo CHtml::link('(Удалить)',array('deluploadfile','id'=>$data['id'],'filename'=>$entry)).'<br>';
		    }
		}
		
		$d->close();?>
	</div>
	
	<?php
		}
	    }
	?>
	

	<!--Блок с кнопками управления-->
	<div class="task_bt">
		<?php 
		echo CHtml::link('(Добавить файл)',array('tasks/uploadfile','id'=>$data['id']));
		
		if (($data['state']==3)&&(Yii::app()->user->checkAccess('leader'))) //Если задача выполнена и ждет подтверждения выводим кнопарь для руководителей
			echo CHtml::link('(Подтвердить)',array('tasks/acceptexecute','id'=>$data['id'],'acpt'=>1));
		
		if (($data['state']!=3)&&($data['state']!=1)&&(Yii::app()->user->checkAccess('manager'))) //Если задача не выполнялась и пользователь менеджер то разрешаем редактировать
			echo CHtml::link('(Редактировать)',array('tasks/update','id'=>$data['id']));
		
		if ((($data['state']!=1)&&(Yii::app()->user->checkAccess('manager')))or(Yii::app()->user->checkAccess('superuser'))){ //Если задача не выполнялась менеджер и суперпользователь может удалять
			echo CHtml::link('(Удалить)',array('tasks/delete','id'=>$data['id']));
			}
		
		if (($data['state']<>1)and($data['state']<>3)and($data['id_executor'] == Yii::app()->user->getId())){ //Если задача не выполнялась и исполнитель текущий пользователь выводим кнопку
			 echo CHtml::link('(Выполнить)',array('tasks/execute','id'=>$data['id'])); }?>
			 
	</div>

</div>
