<?php echo CHtml::beginForm(); ?>
<?php echo CHtml::errorSummary($model); ?>
<?php $dt = explode(" ",$model->startdate);?>
<?php $dt1 = explode(" ",$model->executedate);?>

<br>
<table width=100%>
<tr>
    <td style="width:200px">Первый запуск (Дата/Время)</td>
    <td><?php $this->widget('zii.widgets.jui.CJuiDatePicker', array(
								    'language'=>'ru',
									'name'=>'startdate',
									'value' => $dt[0],
								    'options'=>array(
									'showAnim'=>'fold',
									'dateFormat'=>'yy-mm-dd',
									'buttonImageOnly' => true,
									),
								    'htmlOptions'=>array(
									'maxlength'=>7,
									'size' => 8,
									),
	)); ?>
	&nbsp;&nbsp;/&nbsp;

	<?php
		$this->widget('CMaskedTextField',array(
					'name' => 'starttime',
					'mask'=>'99:99:99',
					'value' => $dt[1],
					'htmlOptions' => array('size' => 6)
		));
	?>
    </td>
</tr>

<tr>
    <td style="width:200px">Выполнить до (Дата/Время)</td>
    <td><?php $this->widget('zii.widgets.jui.CJuiDatePicker', array(
								    'language'=>'ru',
									'name'=>'executedate',
									'value' => $dt1[0],
								    'options'=>array(
									'showAnim'=>'fold',
									'dateFormat'=>'yy-mm-dd',
									'buttonImageOnly' => true,
									),
								    'htmlOptions'=>array(
									'maxlength'=>7,
									'size' => 8,
									),
	)); ?>
	&nbsp;&nbsp;/&nbsp;

	<?php
		$this->widget('CMaskedTextField',array(
					'name' => 'executetime',
					'mask'=>'99:99:99',
					'value' => $dt1[1],
					'htmlOptions' => array('size' => 6)
		));
	?>
    </td>
</tr>


<tr>
	<td><?php echo CHtml::activeLabelEx($model,'id_executor');?></td>
	<td><?php
			//Готовим список выбора пользователей
			$usrlist = CHtml::listData(Users::model()->with('person')->findAll(array('order'=>'person.name','condition'=>"person.id_org=".Yii::app()->user->org,)),'id','person.name');
			$usrlist = array('') + $usrlist;
			echo CHtml::activeDropDownList($model,'id_executor',$usrlist,array('class'=>'text_edit','style'=>'width:400px;'));
		?>
	</td>
</tr>
<tr>
	<td><?php echo CHtml::activeLabelEx($model,'sendsms');?></td>
	<td><?php echo CHtml::activeCheckBox($model,'sendsms'); ?></td>
</tr>
<tr>
	<td><?php echo CHtml::activeLabelEx($model,'id_kontragent');?></td>
	<td><?php
			//Готовим список выбора организаций
			$kntrlist = CHtml::listData(Kontragents::model()->findAll(array('order'=>'name','condition'=>'id_owner='.Yii::app()->user->org.' OR '.'id='.Yii::app()->user->org)),'id','name');
			$kntrlist = array('') + $kntrlist;  //добавляет пустое поле в выподающий список
			echo CHtml::activeDropDownList($model,'id_kontragent',$kntrlist,array('class'=>'text_edit','style'=>'width:400px;'));
		?>
	</td>
</tr>

<tr>
	<td>Уведомлять при выполнении</td>
	<td><?php echo CHtml::activeCheckBox($model,'notify_execute'); ?></td>
</tr>



<tr>
	<td><?php echo CHtml::activeLabelEx($model,'important');?></td>
	<td><?php echo CHtml::activeCheckBox($model,'important'); ?></td>
</tr>

<tr>
	<td><?php echo CHtml::activeLabelEx($model,'accept_execute');?></td>
	<td><?php echo CHtml::activeCheckBox($model,'accept_execute'); ?></td>
</tr>

<tr>
	<td><?php echo CHtml::activeLabelEx($model,'price');?></td>
	<td><?php echo CHtml::activeTextField($model,'price',array('class'=>'text_edit','style'=>'width:100px;')); ?></td>
</tr>

<tr>
	<td><?php echo CHtml::activeLabelEx($model,'description');?></td>
	<td><?php echo CHtml::activeTextArea($model,'description',array('class'=>'text_edit','style'=>'width:400px;')); ?></td>
</tr>

<tr>
	<td colspan=2 align="center"><?php echo chtml::submitbutton('Сохранить',array('class'=>'bt','style'=>'width:90px;')); ?></td>
</tr>

</table>
<?php echo CHtml::endForm(); ?>