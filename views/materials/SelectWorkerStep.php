<?php echo CHtml::beginForm(); ?>
<?php echo CHtml::errorSummary($model); ?>
<?php echo CHtml::activeHiddenField($model,'step');?>

<br>
	<b>[Выбор маршрута перемещения материала]</b> >> Выбор материала
<br>
<br>

<table width=100%>
	<tr>
		<td style="width:150px">Источник: </td>
		<td><?php echo CHtml::activeDropDownList($model,'mat_from',$model->from_list,array('class'=>'text_edit','style'=>'width:400px;')); ?></td>
	</tr>
</table>
<br>
<table width=100%>
	<tr>
		<td style="width:150px">Назначение: </td>
		<td><?php echo CHtml::activeDropDownList($model,'mat_to',$model->to_list,array('class'=>'text_edit','style'=>'width:400px;')); ?></td>
	</tr>
</table>

<center>
	<br>
	<?php echo chtml::submitButton('Далее',array('class'=>'bt','style'=>'width:90px;')); ?>
</center>

<?php echo CHtml::endForm(); ?>