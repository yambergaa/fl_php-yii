<ul class="actions">
	<li></li>
</ul><!-- actions -->
<br>

<?php echo CHtml::beginForm(); ?>
<?php echo CHtml::errorSummary($model); ?>
<?php //echo CHtml::hiddenField('nomenklaturadn');?>
<?php //echo CHtml::hiddenField('nomenklaturaname');?>

<table width=100%>
<tr>
	<td><?php echo CHtml::activeLabelEx($model,'warehouse');?></td>
	<td><?php
			$scladlist = CHtml::listData(ItemsGroups::model()->findAll('type = 3',array('order'=>'name')),'id','name');
			echo CHtml::activeDropDownList($model,'warehouse',$scladlist,array('class'=>'text_edit','style'=>'width:400px;')); 
			?>
	</td>
</tr>
<tr>
	<td style="width:150px">
		<?php echo CHtml::activeLabelEx($model,'name');?>
	</td>
	<td>
		<?php $this->widget('CAutoComplete',
							array(
								'model'=>$model,
								'attribute'=>'name',
								'url'=>array('nomenklatura/autocomplete'),
								'minChars'=>2,
								'htmlOptions'=>array('style'=>'width:400px;'),
									//это ж целый jquery запрос!!!
								//'methodChain'=>'.result(
								//					function(event,item){
								//						$("#nomenklaturaname").val(item[0]);
								//						$("#nomenklaturadn").val(item[1]);
								//					}
								//				)',
							)
					);?>
	</td>
</tr>
<tr>
	<td style="width:150px">
		<?php echo CHtml::activeLabelEx($model,'cnt');?>
	</td>
	<td>
		<?php echo CHtml::activeTextField($model,'cnt',array('class'=>'text_edit','style'=>'width:400px;')); ?>
	</td>
</tr>
<tr>
	<td style="width:150px">
		<?php echo CHtml::activeLabelEx($model,'optprice');?>
	</td>
	<td>
		<?php echo CHtml::activeTextField($model,'optprice',array('class'=>'text_edit','style'=>'width:400px;')); ?>
	</td>
</tr>
<tr>
	<td style="width:150px">
		<?php echo CHtml::activeLabelEx($model,'price');?>
	</td>
	<td>
		<?php echo CHtml::activeTextField($model,'price',array('class'=>'text_edit','style'=>'width:400px;')); ?>
	</td>
</tr>
</table>
<center>
	<?php echo chtml::submitbutton('Сохранить',array('class'=>'bt','style'=>'width:90px;')); ?>
</center>
<?php echo CHtml::endForm(); ?>
