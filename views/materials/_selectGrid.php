<?php
	$this->widget('zii.widgets.grid.CGridView', array(
		//'id'=>'selectgrid',
		'dataProvider'=>$dataProvider,
		'selectableRows'=>0, //Выделение строк (0,1,2)
		'cssFile'=>false,
		'enablePagination'=>true,
		'ajaxUpdate'=>true,
		//-------------  колонки
		'columns'=>array(
			array(
				'class'=>'CDataColumn',
				'header'=>'Материал',
				'name'=>'desc',
			),
			array(
				'class'=>'CDataColumn',
				'header'=>'Цена',
				'name'=>'oprice',
				'htmlOptions' => array('style' => 'text-align: center; width: 20%;'),
			),
		///-----кнопки
			array(
				'class'=>'CButtonColumn',
				'template'=>'{add}{rem}',
				'header'=>'Добавить',
				'buttons'=>array(
					'add' => array(
						'label' => 'Передать материал',
						'imageUrl' => Yii::app()->baseUrl.'/w_comty/images/add2.png',
						'visible' => $mode,
						'url'=> 'Yii::app()->createUrl("/materials/transferto", array("id"=>$data["id"]))',//$row
					),
					'rem' => array(
						'label' => 'Вернуть материал',
						'imageUrl' => Yii::app()->baseUrl.'/w_comty/images/rem1.png',
						'visible' => '!'.$mode,
						'url'=> 'Yii::app()->createUrl("/materials/transferback", array("id"=>$data["id"]))',
					),
				),
			),
		),
	));
?>