<?php
	$this->widget('zii.widgets.grid.CGridView', array(
		'dataProvider'=>$dataProvider,

		'selectableRows'=>1, //Выделение строк (0,1,2)
		'cssFile'=>false,
		'enablePagination'=>true,
		'ajaxUpdate'=>true,
		//-------------  колонки
		'columns'=>array(
			array(
				'header'=>'Наименование',
				'name'=>'name',
				'value'=> 'Materials::model()->findbyPk($data->id_sv)->name',
			),
			
		///-----кнопки
		
			array(
				'class'=>'CButtonColumn',
				'template'=>'{add}',
				'header'=>'Действие',
				'buttons'=>array(
					'add' => array(
						'label' => 'Переместить',						
						'url'=>'Yii::app()->createUrl("materials/addtransfer", array("id"=>$data->id,"type"=>$data->type))',
						'options'=>array(
								'ajax'=>array(
                                                'type'=>'GET',
												//'type'=>'POST',
                                                'url'=>"js:$(this).attr('href')",                                               
												'update'=>'#dropdown_target0',
                                              ),
						),
						
					),
				),
			),
			
			
		),
	));
?>