<?php echo CHtml::beginForm(); ?>
<?php echo CHtml::errorSummary($model); ?>
<?php echo CHtml::textField('kontrdn');?>

<br>
	<b>[Выбор склада]</b> >> Выбор материала
<br>
<br>

<table width=100%>
	<tr>
		<td style="width:150px">Источник: </td>
		<td><?php echo CHtml::activeDropDownList($model,'mat_from',$model->from_list,array('class'=>'text_edit','style'=>'width:400px;')); ?></td>
	</tr>
	<tr>
		<td colspan = 2><br></td>
	</tr>
	<tr>
		<td style="width:150px">
			<?php echo CHtml::activeLabelEx($model,'name');?>
		</td>
		<td>
			<?php $this->widget('CAutoComplete',
								array(
									'model'=>$model,
									'attribute'=>'name',
									'url'=>array('autocompletekontragent'),
									'minChars'=>2,
									'htmlOptions'=>array('style'=>'width:400px;'),
										//это ж целый jquery запрос!!!
									'methodChain'=>'.result(
														function(event,item){
															$("#kontrdn").val(item[1]);
														}
													)',
								)
			);?>
		</td>
	</tr>
</table>

<center>
	<br>
	<?php echo chtml::submitButton('Далее',array('class'=>'bt','style'=>'width:90px;')); ?>
</center>

<?php echo CHtml::endForm(); ?>