<?php

class Tasks extends CActiveRecord{
	const PAGE_SIZE=10;

	//public $startdate;
	//public $starttime;
	public $sendsms;

	/*public function setKontragentID($netid){
  		 self::find(array(
	  		'select'=>'title',
		'condition'=>'postID=:postID',
   		'params'=>array(':postID'=>10),);

	}   */

	public static function model($className=__CLASS__){
		return parent::model($className);
	}

	public function relations()
    {
        return array(
			'executor'=>array(self::BELONGS_TO, 'Users', 'id_executor'),
			'author'=>array(self::BELONGS_TO, 'Users', 'id_author'),
			'kontr'=>array(self::BELONGS_TO, 'Kontragents', 'id_kontragent'), //****
		);
    }


	public function rules(){ //определить правила для данных вводимых пользователем.
		return array(
			array('startdate','required'),
			array('description,stopdate,id_executor,id_owner,notify_execute,
				ex_description,state,important,id_kontragent, sendsms ,price ,accept_execute, executedate','length'), //***********
		);
	}

	public function tableName(){
		return 'tasks';
	}

	public function attributeLabels(){
		return array(
			'startdate' => 'Дата/Время',
			'id_executor' => 'Исполнитель',
			'id_kontragent' => 'Организация',
			'id_owner' => 'Инициатор',
			'description' => 'Описание',
			'important' => 'Важно',
			'sendsms' => 'Оповестить исполнителя',
			'price' => 'Стоимость',
			'accept_execute' => 'Подтверждать выполнение',
		);
	}

}